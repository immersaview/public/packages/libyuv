**Packaging for libyuv**

## Example CMake Usage:
```cmake
target_link_libraries(target yuv)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:yuv,INTERFACE_INCLUDE_DIRECTORIES>")
```